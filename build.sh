aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin 603299147963.dkr.ecr.ap-southeast-2.amazonaws.com
mvn clean install
docker build -t bank-service .
docker tag bank-service:latest 603299147963.dkr.ecr.ap-southeast-2.amazonaws.com/bank-service:latest
docker push 603299147963.dkr.ecr.ap-southeast-2.amazonaws.com/bank-service:latest