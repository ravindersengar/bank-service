package com.xlrs.bank.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class BankBranch extends AbstractEntity{
	
	private static final long serialVersionUID = -5932486282479361294L;
	
	@ManyToOne
	@JoinColumn(name="bank_id")
	@JsonBackReference
	private Bank bank;
	
	private String bankBranchCode;
	
	@ManyToOne
	private Address address;
	
}
