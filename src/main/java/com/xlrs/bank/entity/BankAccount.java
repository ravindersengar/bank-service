package com.xlrs.bank.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class BankAccount extends AbstractEntity{

	
	private static final long serialVersionUID = 4798994433209220130L;
	
	@NotEmpty(message = "{bankaccount.type.mandatory.feild.notempty}")
	//@Enumerated(EnumType.STRING)
	private String accountType;
	
	private String accountId;
	
	private Date openedOn;
	
	@NotEmpty(message = "{bankaccount.userCurrencty.mandatory.feild.notempty}")
	private String userCurrency;
	
	@NotEmpty(message = "{bankaccount.holdertype.mandatory.feild.notempty}")
	//@Enumerated(EnumType.STRING)
	private String accountHolderType;
	
	private Long legalEntityId;
	
	@ManyToOne
	private BankBranch bankBranch;
	
	private String targetSystemBankAccountId;
	
	
}
