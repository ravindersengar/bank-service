package com.xlrs.bank.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Address extends AbstractEntity{
	
	private static final long serialVersionUID = 707081180274498473L;
	
	@NotEmpty(message = "{address.addressLine1.mandatory.feild.notempty}")
	private String name;
	
	@NotEmpty(message = "{address.addressLine2.mandatory.feild.notempty}")
	private String addressLine1;
	
	@NotEmpty(message = "{address.addressLine2.mandatory.feild.notempty}")
	private String addressLine2;
	
	@NotEmpty(message = "{address.city.mandatory.feild.notempty}")
	private String city;
	
	@NotEmpty(message = "{address.state.mandatory.feild.notempty}")
	private String state;
	
	@NotEmpty(message = "{address.zipcode.mandatory.feild.notempty}")
	private String zipCode;
	
	@NotEmpty(message = "{address.countrycode.mandatory.feild.notempty}")
	private String countryCode;
	
}
