package com.xlrs.bank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xlrs.bank.entity.BankAccount;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long>{

	@Query(value = "SELECT ba FROM BankAccount ba WHERE ba.legalEntityId IN :legalEntityIds")
	List<BankAccount> getAllBankAccountByLegalEntityIds(List<Long> legalEntityIds);

}
