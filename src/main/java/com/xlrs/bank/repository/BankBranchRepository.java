package com.xlrs.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.bank.entity.BankBranch;

@Repository
public interface BankBranchRepository extends JpaRepository<BankBranch, Long>{

}
