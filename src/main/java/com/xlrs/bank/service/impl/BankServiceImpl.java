package com.xlrs.bank.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.bank.entity.Bank;
import com.xlrs.bank.repository.BankRepository;
import com.xlrs.bank.service.BankService;
import com.xlrs.bank.view.BankView;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;

@Service
public class BankServiceImpl implements BankService {

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private BankRepository bankRepository;

	@Autowired
	private BaseRepository<Bank> baseRepository;
	
	@Override
	public BankView createBank(BankView createBankView) throws NoResultFoundException {
		Bank bank = createBankView.getId() != null ?  getBankById(createBankView.getId()) : new Bank();
		bank.setCode(createBankView.getCode());
		bank.setName(createBankView.getName());
		bank = bankRepository.save(baseRepository.addAuditFeilds(bank, createBankView.getOrgUserId(), CommonConstants.STATUS_ACTIVE));
		return new BankView(bank.getId(), bank.getName(), bank.getCode(), createBankView.getOrgUserId());
	}

	@Override
	public BankView getBank(Long id) throws NoResultFoundException {
		Bank bank = getBankById(id);
		return new BankView(bank.getId(), bank.getName(), bank.getCode(),null);
	}

	public Bank getBankById(Long id) throws NoResultFoundException {
		Optional<Bank> optional = bankRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		Bank bank = optional.get();
		return bank;
	}

}
