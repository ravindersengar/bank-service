package com.xlrs.bank.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.bank.entity.BankAccount;
import com.xlrs.bank.entity.BankBranch;
import com.xlrs.bank.repository.BankAccountRepository;
import com.xlrs.bank.rest.LegalEntityRestTemplate;
import com.xlrs.bank.service.BankAccountService;
import com.xlrs.bank.service.BankBranchService;
import com.xlrs.bank.view.BankAccountView;
import com.xlrs.bank.view.LegalEntityView;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;

@Service
public class BankAccountServiceImpl implements BankAccountService{

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private BankAccountRepository bankAccountRepository; 
	
	@Autowired
	private BankBranchService bankBranchService;
	
	@Autowired
	private BaseRepository<BankAccount> baseRepository;
	
	@Autowired
	private LegalEntityRestTemplate legalEntityRestTemplate;
	
	@Override
	public BankAccountView createBankAccount(BankAccountView bankAccountView) throws NoResultFoundException {
		BankBranch bankBranch = bankBranchService.getBankBranchById(bankAccountView.getBankBranchId());
		BankAccount bankAccount = bankAccountView.getId() != null ? getBankAccountById(bankAccountView.getId()) : new BankAccount();
		bankAccount.setAccountHolderType(bankAccountView.getAccountHolderType());
		bankAccount.setBankBranch(bankBranch);
		//bankAccount.setAccountType(AccountType.valueOf(bankAccountView.getAccountType()));
		bankAccount.setAccountType(bankAccountView.getAccountType());
		bankAccount.setAccountId(bankAccountView.getAccountId());
		bankAccount.setOpenedOn(new Date());
		bankAccount.setUserCurrency(bankAccountView.getUserCurrency());
		bankAccount.setLegalEntityId(bankAccountView.getLegalEntityId());
		bankAccount.setTargetSystemBankAccountId(bankAccountView.getTargetSystemBankAccountId());
		bankAccount = bankAccountRepository.save(baseRepository.addAuditFeilds(bankAccount, bankAccountView.getOrgUserId(), CommonConstants.STATUS_ACTIVE));
		return new BankAccountView(bankAccount.getAccountType(), bankAccount.getAccountId(), bankAccount.getUserCurrency(), bankAccount.getAccountHolderType(), 
				bankAccount.getLegalEntityId(), bankAccount.getBankBranch().getId(), bankAccount.getTargetSystemBankAccountId(), bankAccount.getBankBranch().getBankBranchCode(),
				bankAccount.getId(), bankAccountView.getOrgUserId(), null);
	}
	
	@Override
	public BankAccountView getBankAccount(Long id) throws NoResultFoundException, ApplicationException {
		BankAccount bankAccount = getBankAccountById(id);
		
		LegalEntityView legalEntityView = legalEntityRestTemplate.getLegalEntityById(bankAccount.getLegalEntityId());
		return new BankAccountView(bankAccount.getAccountType(), bankAccount.getAccountId(), bankAccount.getUserCurrency(), 
				bankAccount.getAccountHolderType(), bankAccount.getLegalEntityId(), bankAccount.getBankBranch().getId(), 
				bankAccount.getTargetSystemBankAccountId(), bankAccount.getBankBranch().getBankBranchCode(), bankAccount.getId(), null, legalEntityView.getName());
	}

	private BankAccount getBankAccountById(Long id) throws NoResultFoundException {
		Optional<BankAccount> optional = bankAccountRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		BankAccount bankAccount = optional.get();
		return bankAccount;
	}

	@Override
	public List<BankAccountView> getAllBankAccountByLegalEntityIds(List<Long> legalEntityIds) {
		List<BankAccount> bankAccounts = bankAccountRepository.getAllBankAccountByLegalEntityIds(legalEntityIds); 
		List<BankAccountView> bankAccountsView = new ArrayList<BankAccountView>();
		
		BankAccountView bav = null;
		for(BankAccount ba:bankAccounts) {
			bav = new BankAccountView(ba.getAccountType(), ba.getAccountId(), ba.getUserCurrency(), ba.getAccountHolderType(),
					ba.getLegalEntityId(), ba.getBankBranch().getId(), ba.getTargetSystemBankAccountId(), 
					ba.getBankBranch().getBankBranchCode(), ba.getId(), null, null);
			bankAccountsView.add(bav);
		}
		return bankAccountsView;
	}

}
