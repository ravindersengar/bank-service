package com.xlrs.bank.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.bank.entity.Address;
import com.xlrs.bank.repository.AddressRepository;
import com.xlrs.bank.service.AddressService;
import com.xlrs.bank.view.AddressView;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;

@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private AddressRepository addressRepository; 
	
	@Autowired
	private BaseRepository<Address> baseRepository;
	
	@Override
	public AddressView createAddress(AddressView addressView)  throws NoResultFoundException {
		Address address = addressView.getId() != null ? getAddressById(addressView.getId()) : new Address();
		address.setName(addressView.getName());
		address.setAddressLine1(addressView.getAddressLine1());
		address.setAddressLine2(addressView.getAddressLine2());
		address.setCity(addressView.getCity());
		address.setState(addressView.getState());
		address.setZipCode(addressView.getZipCode());
		address.setCountryCode(addressView.getCountryCode());
		address = addressRepository.save(baseRepository.addAuditFeilds(address, addressView.getOrgUserId(), CommonConstants.STATUS_ACTIVE));
		return new AddressView(address.getName(), address.getAddressLine1(), address.getAddressLine2(), address.getCity(), address.getState(), address.getZipCode(), address.getCountryCode(), address.getId(), addressView.getOrgUserId());
	}

	@Override
	public AddressView getAddress(Long id) throws NoResultFoundException {
		Address address = getAddressById(id);
		return new AddressView(address.getName(), address.getAddressLine1(), address.getAddressLine2(), address.getCity(), address.getState(), address.getZipCode(), address.getCountryCode(), address.getId(), null);
	}

	public Address getAddressById(Long id) throws NoResultFoundException {
		Optional<Address> optional = addressRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		Address address = optional.get();
		return address;
	}

}
