package com.xlrs.bank.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.bank.entity.Address;
import com.xlrs.bank.entity.Bank;
import com.xlrs.bank.entity.BankBranch;
import com.xlrs.bank.repository.BankBranchRepository;
import com.xlrs.bank.service.AddressService;
import com.xlrs.bank.service.BankBranchService;
import com.xlrs.bank.service.BankService;
import com.xlrs.bank.view.BankBranchView;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;

@Service
public class BankBranchServiceImpl implements BankBranchService{

	@Autowired
	private MessageSource messages;
	
	@Autowired
	private BankBranchRepository bankBranchRepository;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private BaseRepository<BankBranch> baseRepository;
	
	@Override
	public BankBranchView createBankBranch(BankBranchView bankBranchView) throws NoResultFoundException {
		Bank bank = bankService.getBankById(bankBranchView.getBankId());
		Address address = addressService.getAddressById(bankBranchView.getAddressId());
		BankBranch bankBranch = bankBranchView.getId() != null ? getBankBranchById(bankBranchView.getId()) : new BankBranch();
		bankBranch.setBankBranchCode(bankBranchView.getBankBranchCode());
		bankBranch.setBank(bank);
		bankBranch.setAddress(address);
		bankBranch = bankBranchRepository.save(baseRepository.addAuditFeilds(bankBranch, bankBranchView.getOrgUserId(), CommonConstants.STATUS_ACTIVE));
		return new BankBranchView(bank.getId(), bankBranch.getBankBranchCode(), bankBranch.getAddress().getId(), bankBranch.getId(), bankBranchView.getOrgUserId());
	}

	@Override
	public BankBranchView getBankBranch(Long id) throws NoResultFoundException {
		BankBranch bankBranch = getBankBranchById(id);
		return new BankBranchView(bankBranch.getBank().getId(), bankBranch.getBankBranchCode(), bankBranch.getAddress().getId(), bankBranch.getId(), null);
	}

	public BankBranch getBankBranchById(Long id) throws NoResultFoundException {
		Optional<BankBranch> optional = bankBranchRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		BankBranch bankBranch = optional.get();
		return bankBranch;
	}

}
