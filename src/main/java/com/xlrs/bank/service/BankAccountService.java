package com.xlrs.bank.service;

import java.util.List;

import com.xlrs.bank.view.BankAccountView;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;

public interface BankAccountService {

	public BankAccountView createBankAccount(BankAccountView bankAccountView) throws NoResultFoundException ;

	public BankAccountView getBankAccount(Long id) throws NoResultFoundException, ApplicationException ;

	public List<BankAccountView> getAllBankAccountByLegalEntityIds(List<Long> legalEntityIds);

	
}
