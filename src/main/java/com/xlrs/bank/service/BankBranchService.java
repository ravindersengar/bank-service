package com.xlrs.bank.service;

import com.xlrs.bank.entity.BankBranch;
import com.xlrs.bank.view.BankBranchView;
import com.xlrs.commons.exception.NoResultFoundException;

public interface BankBranchService {

	public BankBranchView createBankBranch(BankBranchView bankBranchView) throws NoResultFoundException ;

	public BankBranchView getBankBranch(Long id) throws NoResultFoundException ;
	
	public BankBranch getBankBranchById(Long id) throws NoResultFoundException;
	
}
