package com.xlrs.bank.service;

import com.xlrs.bank.entity.Address;
import com.xlrs.bank.view.AddressView;
import com.xlrs.commons.exception.NoResultFoundException;

public interface AddressService {

	public AddressView createAddress(AddressView addressView) throws NoResultFoundException ;

	public AddressView getAddress(Long id) throws NoResultFoundException ;

	public Address getAddressById(Long id) throws NoResultFoundException;
}
