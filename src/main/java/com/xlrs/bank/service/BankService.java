package com.xlrs.bank.service;

import com.xlrs.bank.entity.Bank;
import com.xlrs.bank.view.BankView;
import com.xlrs.commons.exception.NoResultFoundException;

public interface BankService {

	public BankView createBank(BankView createBankView) throws NoResultFoundException ;

	public BankView getBank(Long id) throws NoResultFoundException ;
	
	public Bank getBankById(Long id) throws NoResultFoundException;
	
}
