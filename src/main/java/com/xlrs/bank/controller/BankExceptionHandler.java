package com.xlrs.bank.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.xlrs.commons.controller.AbstractRestHandler;

/**
 * This class is meant to be extended by all REST resource "controllers". It
 * contains exception mapping and other common REST API functionality
 */
@ControllerAdvice
public class BankExceptionHandler extends AbstractRestHandler{
	
}