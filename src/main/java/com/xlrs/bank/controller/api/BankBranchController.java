package com.xlrs.bank.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.bank.service.BankBranchService;
import com.xlrs.bank.view.BankBranchView;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/bank/branch")
@Slf4j
public class BankBranchController {

	@Autowired
	private BankBranchService bankBranchService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createBankBranch(@RequestBody BankBranchView bankBranchView) throws Exception {
		log.debug("Requested payload is : " + bankBranchView);
		
		try {
			return new ResponseView(bankBranchService.createBankBranch(bankBranchView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getAddress(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(bankBranchService.getBankBranch(id));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.result.not.found", null, null), e.getMessage(),e);
		}
	}
}
