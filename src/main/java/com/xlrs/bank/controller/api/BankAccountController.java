package com.xlrs.bank.controller.api;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.bank.service.BankAccountService;
import com.xlrs.bank.view.BankAccountView;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.exception.RequestValidationException;
import com.xlrs.commons.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/bank/account")
@Slf4j
public class BankAccountController  {

	@Autowired
	private BankAccountService bankAccountService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createBankAccount(@RequestBody BankAccountView bankAccountView) throws Exception {
		log.debug("Requested payload is : " + bankAccountView);
		
		try {
			return new ResponseView(bankAccountService.createBankAccount(bankAccountView));
		}catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getBankAccount(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(bankAccountService.getBankAccount(id));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.result.not.found", null, null), e.getMessage(),e);
		}
	}
	
	
	@PostMapping("/legalentities/allbankaccounts")
	@ResponseBody
	public ResponseView getAllBankAccountByLegalEntityIds(@RequestBody Long[] legalEntityIds ) throws Exception {
		try {
			if(legalEntityIds==null || legalEntityIds.length==0) {
				throw new RequestValidationException(messages.getMessage("err.invalid.payload.type", null, null));
			}
			List<Long> entityIds = Arrays.asList(legalEntityIds);
			return new ResponseView(bankAccountService.getAllBankAccountByLegalEntityIds(entityIds));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.result.not.found", null, null), e.getMessage(),e);
		}
	}
}
