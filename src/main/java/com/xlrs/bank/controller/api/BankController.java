package com.xlrs.bank.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.bank.service.BankService;
import com.xlrs.bank.view.BankView;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/bank")
@Slf4j
public class BankController {

	@Autowired
	private BankService bankService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createBank(@RequestBody BankView createBankView) throws Exception {
		log.debug("Requested payload is : " + createBankView);
		
		try {
			return new ResponseView(bankService.createBank(createBankView));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.bank.not.created", null, null), e.getMessage(),e);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getAddress(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(bankService.getBank(id));
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.result.not.found", null, null), e.getMessage(),e);
		}
	}
}
