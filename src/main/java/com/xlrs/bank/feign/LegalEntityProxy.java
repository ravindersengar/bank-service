package com.xlrs.bank.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xlrs.commons.view.ResponseView;

@FeignClient(name = "organisation-service" , url = "${legalEntity.get.url}")
public interface LegalEntityProxy {
	@GetMapping("/{id}")
	public ResponseView getLegalEntity(@PathVariable Long id) throws Exception;
}
