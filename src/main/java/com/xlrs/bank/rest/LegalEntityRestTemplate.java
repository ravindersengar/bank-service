package com.xlrs.bank.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.bank.feign.LegalEntityProxy;
import com.xlrs.bank.view.LegalEntityView;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LegalEntityRestTemplate {
	
	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;

	@Value("${legalEntity.get.url}")
	private String getLegalEntityURL;
	
	@Autowired
	private LegalEntityProxy legalEntityProxy;
	
	@Autowired
	private MessageSource messages;

	public LegalEntityView getLegalEntityById(Long legalEntityId) throws ApplicationException {
		LegalEntityView legalEntityView =null;
		try {
			if(getLegalEntityURL == null || "".equals(getLegalEntityURL)) {
				throw new ApplicationException("URL found Null");
			}
			log.info("calling org service url ::::::::::::: " + getLegalEntityURL);
//			ResponseView responseView = restServiceBuilder.getWithHeader(getLegalEntityURL, ResponseView.class, null, legalEntityId);
			ResponseView responseView = legalEntityProxy.getLegalEntity(legalEntityId);
			//ResponseView responseView = (ResponseView) response.getBody();
			if(responseView.getErrorMessages()==null) {
				legalEntityView =  new ObjectMapper().convertValue(responseView.getData(), LegalEntityView.class);
			}
			log.info("Getting LegalEntity by legalEntity-id" + legalEntityView.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException(messages.getMessage("err.service.system.error.message", null, null), e.getMessage(),e);
		}
		return legalEntityView;
	}

}
