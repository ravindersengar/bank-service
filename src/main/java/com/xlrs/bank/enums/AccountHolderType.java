package com.xlrs.bank.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum AccountHolderType {
	//joint, single
	
	JOINT("joint"),
	SINGLE("single");
	
	@Getter private String value;
}
