package com.xlrs.bank.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum AccountType {
//	transaction, savings, credit card
	TRANSACTION("transaction"),
	SAVINGS("savings"),
	CREADIT_CARD("credit card");
	
	@Getter private String value;
}
