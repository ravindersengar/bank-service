package com.xlrs.bank.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BankBranchView {
	private Long bankId;
	//@NotEmpty(message = "{bankbranch.bankBranchCode.mandatory.feild.notempty}")
	private String bankBranchCode;
	private Long addressId;
	
	private Long id;
	private Long orgUserId;
}
