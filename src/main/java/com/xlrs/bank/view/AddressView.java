package com.xlrs.bank.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AddressView {
	@NotEmpty(message = "{address.name.mandatory.feild.notempty}")
	private String name;
	@NotEmpty(message = "{address.addressLine1.mandatory.feild.notempty}")
	private String addressLine1;
	@NotEmpty(message = "{address.addressLine2.mandatory.feild.notempty}")
	private String addressLine2;
	@NotEmpty(message = "{address.city.mandatory.feild.notempty}")
	private String city;
	@NotEmpty(message = "{address.state.mandatory.feild.notempty}")
	private String state;
	@NotEmpty(message = "{address.zipcode.mandatory.feild.notempty}")
	private String zipCode;
	@NotEmpty(message = "{address.countrycode.mandatory.feild.notempty}")
	private String countryCode;
	
	private Long id;
	private Long orgUserId;
}
