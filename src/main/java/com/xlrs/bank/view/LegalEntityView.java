package com.xlrs.bank.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class LegalEntityView implements BaseView{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2364686771630930909L;
	
	@NotEmpty(message = "{legal.entity.name.mandatory.feild.notempty}")
	private String name;
	private Long organisationId;
	private String organisationName;
	private Long id;
}
