package com.xlrs.bank.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BankAccountView implements BaseView{
	
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{bankaccount.type.mandatory.feild.notempty}")
	private String accountType;
	private String accountId;
	@NotEmpty(message = "{bankaccount.userCurrencty.mandatory.feild.notempty}")
	private String userCurrency;
	@NotEmpty(message = "{bankaccount.holdertype.mandatory.feild.notempty}")
	private String accountHolderType;
	private Long legalEntityId;
	private Long bankBranchId;
	private String targetSystemBankAccountId;
	@NotEmpty(message = "{bankaccount.code.mandatory.feild.notempty}")
	private String bankBranchCode;
	
	private Long id;
	private Long orgUserId;
	
	private String legalEntityName;
}
