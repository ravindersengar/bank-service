package com.xlrs.bank.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BankView {

		private Long id;
		@NotEmpty(message = "{bank.name.mandatory.feild.notempty}")
		private String name;
		@NotEmpty(message = "{bank.code.mandatory.feild.notempty}")
		private String code;
		
		private Long orgUserId;
}
