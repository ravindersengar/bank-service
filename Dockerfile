FROM openjdk:8-jdk-alpine
EXPOSE 4004
VOLUME /main-app
ADD /target/bank-service-0.1.0.jar bank-service-0.1.0.jar
ENTRYPOINT ["java","-jar","bank-service-0.1.0.jar"]